var config = {
  entry: './src/main.js',

  output: {
    path:'./dist/',
    filename: 'build.js',
  },

  devServer: {
    inline: true,
    port: 8080
  },

  module: {
    loaders: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        loader: 'babel',

        query: {
          presets: ['es2015', 'react', 'stage-3']
        }
      }
    ]
  }
};

module.exports = config;