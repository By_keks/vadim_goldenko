export const Products = [
    {
        name: "BRAND NAME HERE",
        description: "Eyelet detail sweatshirt",
        price: {
            old: "",
            new: "$180"
        },
        imgSrc: "src/img/thumb1.png",
        sale: false
    },
    {
        name: "FARFETCH",
        description: "Printed tee",
        price: {
            old: "$70",
            new: "$35"
        },
        imgSrc: "src/img/thumb2.png",
        sale: true
    },
    {
        name: "FOREVER 21",
        description: "Contemporary collared ...",
        price: {
            old: "",
            new: "$15.90"
        },
        imgSrc: "src/img/thumb3.png",
        sale: false
    },
    {
        name: "RAG & BONE",
        description: "Luna top",
        price: {
            old: "",
            new: "$150"
        },
        imgSrc: "src/img/thumb4.png",
        sale: false
    },
    {
        name: "BRAND NAME HERE",
        description: "Eyelet detail sweatshirt",
        price: {
            old: "",
            new: "$180"
        },
        imgSrc: "src/img/thumb5.png",
        sale: false
    },
    {
        name: "TOPSHOP",
        description: "Looney Tunes crop top",
        price: {
            old: "",
            new: "$50"
        },
        imgSrc: "src/img/thumb6.png",
        sale: false
    },
];