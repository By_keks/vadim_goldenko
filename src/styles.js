export const Styles = {
    root: {
        display: 'flex',
        flexDirection: 'column'
    },
    header: {
        flex: '1',
        flexWrap: 'wrap',
        flexDirection: 'column',
        backgroundColor: 'rgb(245,247,250)'
    },
    topline: {
        padding: '15px 10px',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    toplineImg: {
        marginRight: '5px'
    },
    backToCategories: {
        alignItems: 'center',
        flex: '2 1',
        fontSize: '16px'
    },
    toplineHead: {
        alignItems: 'center',
        flex: '5 2',
        fontWeight: 'bold',
        justifyContent: 'center'
    },
    toplineHeadImg: {
        height: '6px',
        marginLeft: '5px'
    },
    toplineLock: {
        alignItems: 'center',
        flex: '2 1',
        justifyContent: 'flex-end'
    },
    filtersContainer: {
        borderTopColor: '#e2e7ea',
        backgroundColor: '#fff',
        borderStyle: 'solid',
        borderWidth: '0',
        borderTopWidth: '1px',
        borderBottomWidth: '1px',
        borderBottomColor: 'rgba(133,133,133,0.12)',
    },
    filterDefault: {
        flexWrap: 'wrap',
        flexDirection: 'column',
        flex: '5 1',
        padding: '3px 10px',
        borderRightColor: '#e2e7ea',
        borderStyle: 'solid',
        borderWidth: '0',
        borderRightWidth: '1px',
        alignItems: 'flex-start'
    },
    filterDefaultTitle: {
        alignItems: 'center',
        justifyContent: 'space-between',
        fontSize: '12px',
        color: 'rgb(187,199,205)',
        alignSelf: 'stretch'
    },
    filterDefaultValue: {
        fontSize: '10px',
        fontStyle: 'italic'
    },
    filterDefaultValueTriangle: {
        height: '6px'
    },
    filterRight: {
        flex: '3 1',
    },
    filterRightTitle: {
        alignItems: 'center',
        paddingLeft: '5px'
    },
    search: {
        padding: '10px 12px 0'
    },
    searchForm: {
        justifyContent: 'center',
        position: 'relative',
        flex: '1'
    },
    searchField: {
        flex: '1',
        height: '28px',
        paddingLeft: '32px',
        borderColor: 'transparent',
        borderRadius: '8px',
        fontStyle: 'italic',
        letterSpacing: '0.5px',
        fontSize: '13px'
    },
    submitField: {
        position: 'absolute',
        left: '0',
        top: '0',
        height: '28px',
        backgroundColor: 'transparent',
        borderColor: 'transparent',
        padding: '0 2px'
    },
    main: {
        backgroundColor: '#f5f7fa',
        padding: '0 0 10px'
    },
    productList: {
        flexWrap: 'wrap',
        padding: '5px 8px 0',
        justifyContent: 'space-between'
    },
    addButton: {
        position: 'absolute',
        right: '3px',
        top: '3px',
        width: '22px',
        height: '22px'
    },
    productSale: {
        position: 'absolute',
        left: '0',
        top: '8px'
    },
    product: {
        width: '170px',
        flexWrap: 'wrap',
        flexDirection: 'column',
        position: 'relative',
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: 'rgba(133,133,133,0.12)',
        backgroundColor: '#fff',
        margin: '10px 0 0',
        fontSize: '11px',
        alignItems: 'center',
        padding: '0 0 3px'
    },
    productThumb: {
        height: '130px',
        justifyContent: 'center',
        padding: '3px 0 6px',
        alignItems: 'flex-end',
        fontSize: '0'
    },
    productImg: {
        maxHeight: '130px',
        maxWidth: '100px'
    },
    productName: {
        fontWeight: '500'
    },
    productDescription: {
        lineHeight: '14px'
    },
    productPrices: {
        lineHeight: '16px',
        alignItems: 'flex-end',
        paddingTop: '3px'
    },
    productPricesOnlyNew: {
        lineHeight: '19px'
    },
    productPriceNew: {
        fontSize: '14px',
        fontWeight: 'bold',
    },
    productPriceNewAfterOld: {
        fontSize: '14px',
        fontWeight: 'bold',
        color: 'rgb(252,112,112)'
    },
    productPriceOld: {
        fontSize: '12px',
        fontWeight: 'bold',
        marginRight: '5px',
        lineHeight: '14px',
        textDecoration: 'line-through'
    },

};