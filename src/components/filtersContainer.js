import React from 'react'

import { Styles } from '../styles'

export class FiltersContainer extends React.Component {
    render() {
        return(
            <div style={Styles.filtersContainer}>
                <div style={Styles.filterDefault}>
                    <div style={Styles.filterDefaultTitle}>
                        Brands
                        <img style={Styles.filterDefaultValueTriangle}  src="src/img/triangle48.png" />
                    </div>
                    <div style={Styles.filterDefaultValue}>All</div>
                </div>
                <div style={Styles.filterDefault}>
                    <div style={Styles.filterDefaultTitle}>
                        Retailers
                        <img style={Styles.filterDefaultValueTriangle}  src="src/img/triangle48.png" />
                    </div>
                    <div style={Styles.filterDefaultValue}>$300</div>
                </div>
                <div style={Styles.filterDefault}>
                    <div style={Styles.filterDefaultTitle}>
                        Price
                        <img style={Styles.filterDefaultValueTriangle}  src="src/img/triangle48.png" />
                    </div>
                    <div style={Styles.filterDefaultValue}>All</div>
                </div>
                <div style={Styles.filterDefault}>
                    <div style={Styles.filterDefaultTitle}>
                        Color
                        <img style={Styles.filterDefaultValueTriangle} src="src/img/triangle48.png" />
                    </div>
                    <div style={Styles.filterDefaultValue}>All</div>
                </div>
                <div style={Styles.filterRight}>
                    <div style={Styles.filterRightTitle}>
                        <img src="src/img/group38.png" />
                        <img src="src/img/group38_2.png" />
                    </div>
                </div>
            </div>
        );
    }
}