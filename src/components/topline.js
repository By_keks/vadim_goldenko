import React from 'react'

import { Styles } from '../styles'

export class Topline extends React.Component {
    render() {
        return (
            <div style={Styles.topline}>
                <div style={Styles.backToCategories}>
                    <img style={Styles.toplineImg} src="src/img/path192.png" alt="" />
                    Categories
                </div>
                <div style={Styles.toplineHead}>
                    SELECTED
                    <img style={Styles.toplineHeadImg} src="src/img/triangle48.png" />
                </div>
                <div style={Styles.toplineLock}>
                    <img style={Styles.toplineLockImg} src="src/img/lock04.png" />
                </div>
            </div>
        );
    }
}