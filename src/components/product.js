import React from 'react'

import { Styles } from '../styles'

export class ProductOne extends React.Component {
    render() {
        let data = this.props.data;
        return (
            <div style={Styles.product}>
                {
                    data.sale ? 
                        <div style={Styles.productSale}>
                            <img src="src/img/saleTag.png" alt="" />
                        </div>
                    :
                        ''
                }
                <div style={Styles.addButton}>
                    <img src="src/img/addButton.png" alt="" />
                </div>
                <div style={Styles.productThumb}>
                    <img style={Styles.productImg} src={data.imgSrc} alt="" />
                </div>
                <div style={Styles.productName}>{data.name}</div>
                <div style={Styles.productDescription}>{data.description}</div>
                {
                    data.price.old ?
                        <div style={Styles.productPrices}>
                            <span style={Styles.productPriceOld}>{data.price.old}</span>
                            <span style={Styles.productPriceNewAfterOld}>{data.price.new}</span>
                        </div>
                    :
                        <div style={Styles.productPricesOnlyNew}>
                            <span style={Styles.productPriceNew}>{data.price.new}</span>
                        </div>
                }
            </div>
        );
    }
}