import React from 'react'

import { Styles } from '../styles'

export class Search extends React.Component {
    render() {
        return (
            <div style={Styles.search}>
                <div style={Styles.searchForm}>
                    <input style={Styles.searchField} type="search" placeholder="Looking for something specific?" />
                    <button style={Styles.submitField} value="">
                        <img src="src/img/group4.png" />
                    </button>
                </div>
            </div>
        );
    }
}