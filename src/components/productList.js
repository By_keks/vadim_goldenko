import React from 'react'
import { ProductOne } from './product'

import { Styles } from '../styles'

export class ProductList extends React.Component {
    render() {
        var data = this.props.items;
        var productTemplate = data.map(function(p, i) {
            return (
                <ProductOne key={i} data={p}></ProductOne>
            );
        });
        return (
            <div style={Styles.productList}>
                {productTemplate}
            </div>
        );
    }
}