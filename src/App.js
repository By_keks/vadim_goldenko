import React from 'react'
import { ProductList, FiltersContainer, Topline, Search } from './components'

import { Styles } from './styles'

import { Products } from './data/products'

export default class App extends React.Component {
    render() {
        return (
            <div style={Styles.root}>
                <div style={Styles.header}>
                    <Topline></Topline>
                    <FiltersContainer></FiltersContainer>
                    <Search></Search>
                </div>
                <div style={Styles.main}>
                    <ProductList items={Products}></ProductList>
                </div>
            </div>
        );
    }
}
